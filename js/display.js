$(function () {

  // globaal variables

  var MAX_DISCONNECT_COUNT = 2;
  var SERVER_POLL_INTERVAL = 500;

  var current_room = null;
  var username = null;
  var roomMessages = {};
  var roomNewCount = {};
  var disconnectCount = 0;

  // initialize
  ///////////////////////////////////////////////////////////////////////////

  var chatSource   = $("#chat-template").html();
  var chatTemplate = Handlebars.compile(chatSource);
  var msgSource   = $("#msg-template").html();
  var msgTemplate = Handlebars.compile(msgSource);
  var roomSource   = $("#room-template").html();
  var roomTemplate = Handlebars.compile(roomSource);
  var introSource   = $("#intro-template").html();
  var introTemplate = Handlebars.compile(introSource);

  $('.modal').on('shown.bs.modal', function () {
    $(this).find('input').focus()
  });

  $('[data-toggle="tooltip"]').tooltip();

  // events
  ///////////////////////////////////////////////////////////////////////////

  $('#login-form input').focus();

  //check server status
  ChatApp.connected(function(){
    $('#login-username').closest('.form-group').removeClass('has-error');
    $("#chat-message-form :input").attr("disabled", false);
  });

  ChatApp.disconnected(function(){
    $('#login-username').closest('.form-group').addClass('has-error');
    $('#connection-lost').show();
    $("#chat-message-form :input").attr("disabled", true);
    ChatApp.failover(function() {
      $('#login-username').closest('.form-group').removeClass('has-error');
      $('#connection-lost').hide();
      $("#chat-message-form :input").attr("disabled", false);
    });
  });

  $('#login-form').submit(function (e) {
    e.preventDefault();
    username = $('#login-username').val();

    // load joined room from server
    ChatApp.initRoomHandler(function (roomNames) {
      console.log(roomNames);
      $('#room-list .room').remove();
      roomNames.forEach(function (room) {
        showRoom(room);
      });
    });

    // on message in
    ChatApp.messageHandler(function (time, from, rcvRoom, msg) {
      // console.log(roomMessages, time, from, rcvRoom, msg);
      if(!(rcvRoom in roomMessages))
      roomMessages[rcvRoom] = [];
      roomMessages[rcvRoom].push({
        username: from,
        msg: msg,
        time: time
      });
      if (current_room === rcvRoom) {
        setReadAll(rcvRoom);
        showMessages(rcvRoom);
      } else {
        incrUnread(rcvRoom);
      }
    });

    // code that should run after log in goes here
    ChatApp.setUsername(username, function (success) {
      maxTime = 0;
      console.log(roomMessages);
      for (var k in roomMessages) {
        room = roomMessages[k];
        if(room.length > 0 && room[room.length - 1].time > maxTime){
          maxTime = room[room.length - 1].time;
        }
      }
      ChatApp.sendLastTimeStamp(maxTime);

      if (success) {
        $('.login-page').fadeOut();
        $('#username').text(username);

        // register event listeners
        registerRoomClickHandler();

        $('#logout-btn').click(function (e) {
          e.preventDefault();
          location.reload();
        });

        $('#create-room-form').submit(function (e) {
          e.preventDefault();
          var newRoom = $('#new-room-name').val();
          $('#new-room-name').val('');
          createRoom(newRoom);
          $(this).closest('.modal').modal('hide');
          console.log("create room " + newRoom);
        });

        $('#join-room-form').submit(function (e) {
          e.preventDefault();
          var newRoom = $('#join-room-name').val();
          $('#join-room-name').val('');
          joinRoom(newRoom);
          $(this).closest('.modal').modal('hide');
          console.log("join room " + newRoom);
        });
      }
    });
  });

  // functions
  ///////////////////////////////////////////////////////////////////////////

  function registerRoomClickHandler() {
    $('.room').off().click(function () {
      current_room = $(this).data('room-id')
      openRoom(current_room);
      console.log("in room " + current_room);
    });
  }

  function openRoom(roomId) {
    // runs when the room is selected
    current_room = roomId;

    $('.room').removeClass('active');

    var roomBtn = $('.room[data-room-id="' + roomId + '"]')
    roomBtn.addClass('active');
    var chat = $(chatTemplate({
      roomId: roomId,
      roomName: roomBtn.find('.room-name').text()
    }));
    $('#main-box').html(chat.hide().fadeIn());

    // TODO load room messages here
    showMessages(roomId);
    setReadAll(roomId);

    // event listener for each room
    $('#chat-message-form').off().submit(function (e) {
      e.preventDefault();
      sendMessage($('#chat-text').val());
    });

    $('#leave-room').off().click(function(){
      console.log('leave room: ' + current_room);
      leaveRoom(roomId);
    });

    $('#pause-room').off().click(function(){
      console.log('pause room: ' + current_room);
      ChatApp.pause(current_room, function (success) {
        if (success) {
          $('#paused').show();
          roomBtn.find('.paused').show();
          $('#chat-area').addClass('paused');
        }
      });
    });

    $('#resume-room').off().click(function(){
      console.log('resume room: ' + current_room);
      // send 'getUnead' message to server
      ChatApp.resume(current_room, function (success) {
        if (success) {
          $('#paused').hide();
          roomBtn.find('.paused').hide();
          $('#chat-area').removeClass('paused');
          showMessages(current_room);
        }
      });
    });
  }

  function addMessage(username, message, time, self, server) {
    $('#chat-area').append(msgTemplate({
      username: username,
      message: message,
      time: moment(time).format('D/M/YY HH:mm:ss'),
      self: self,
      server: server === true
    }));
    var chatArea = $("#chat-area");
    chatArea.scrollTop(chatArea.prop("scrollHeight"));
  }

  function showMessages(roomId) {
    var m = roomMessages[roomId];
    $('#chat-area').empty();
    m.sort(function(a, b){
      if(a.time < b.time) return -1;
      if(a.time > b.time) return 1;
      return 0;
    });
    for (var i = 0; i < m.length; i++) {
      addMessage(m[i].username, m[i].msg, m[i].time, m[i].username === username, m[i].username === '_server');
    }
  }

  function sendMessage(message) {
    message.trim();
    if (message.length > 0) {
      $('#chat-text').val('');
      console.log('sending "' + message + '" to room ' + current_room);
      ChatApp.send(current_room, message, function (success) { /* do something */ });
    }
  }

  function createRoom (newRoom) {
    ChatApp.join(newRoom, function(success){
      if(success){
        $('#room-list').append(roomTemplate({
          roomId: newRoom,
          roomName: newRoom,
          newMessages: 0
        }));
        setReadAll(newRoom);
        if(!(newRoom in roomMessages))
        roomMessages[newRoom] = [];
        openRoom(newRoom);
        registerRoomClickHandler();
      }
    });
  }

  function joinRoom (newRoom) {
    ChatApp.join(newRoom, function(success){
      if(success){
        $('#room-list').append(roomTemplate({
          roomId: newRoom,
          roomName: newRoom,
          newMessages: 0
        }));
        // if old messages needed to be loaded, load here
        setReadAll(newRoom);
        if(!(newRoom in roomMessages))
        roomMessages[newRoom] = [];
        openRoom(newRoom);
        registerRoomClickHandler();
      }
    });
  }

  function showRoom(roomId) {
    $('#room-list').append(roomTemplate({
      roomId: roomId,
      roomName: roomId,
      newMessages: 0
    }));
    // if old messages needed to be loaded, load here
    setReadAll(roomId);
    if(!(roomId in roomMessages))
    roomMessages[roomId] = [];
    registerRoomClickHandler();
  }

  function leaveRoom (roomId) {
    ChatApp.leave(current_room, function (success) {
      if(success){
        var intro = $(introTemplate());
        $('#main-box').html(intro);
        $('#room-list [data-room-id="' + roomId + '"]').fadeOut().remove();
        roomMessages[roomId] = [];
        intro.hide().fadeIn();
      }
    });
  }

  function setReadAll(roomId) {
    roomNewCount[roomId] = 0;
    $('#room-list [data-room-id="' + roomId + '"]').find('.unread').hide();
  }

  function incrUnread(roomId) {
    roomNewCount[roomId]++;
    $('#room-list [data-room-id="' + roomId + '"]').find('.unread').text(roomNewCount[roomId]).show();
  }

});
