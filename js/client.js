/**
* Created by innocenat on 2016-04-17.
*/

/*
Also include this tag
<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
*/

/*

Example:

ChatApp.setUsername('foo');
ChatApp.messageHandler(function (time, from, room, msg) {
// Do whatever
});
ChatApp.join('hello-world');
ChatApp.send('hello-world', 'Hello from foo!', function(r) {});

*/

var PING_INTERVAL = 2000;
// var SERVER_LIST = [
//   'http://localhost:3001',
//   'http://localhost:3002'
// ];

var SERVER_LIST = [
  'http://10.10.11.213:3001',
  'http://10.10.11.213:3002'
];

var ChatApp = {};
var current_server = 0;

(function(io, ChatApp) {

  var socket = io(SERVER_LIST[current_server]); // Default path
  var saved_username = '__';
  var mH, rH;

  // var socket = io('http://localhost:3001'); // Default path
  ChatApp.isConnected = function() {
    return socket.connected;
  };

  ChatApp.connected = function(handler) {
    socket.on('connect', handler);
  };

  ChatApp.disconnected = function(handler) {
    socket.on('disconnect', handler);
  };

  ChatApp.failover = function(comebackHandler) {
    current_server++;
    if (current_server >= SERVER_LIST.length)
        current_server = 0;
    socket.disconnect();
    socket = io(SERVER_LIST[current_server]);
    socket.on('message', mH);
    socket.on('init-room', rH);
    socket.emit('username', saved_username, function() {
      comebackHandler();
    });
    // socket.emit('last-timestamp', saved_username, function() {});
  };

  ChatApp.initRoomHandler = function(handler) {
    // Handler prototype function (room)
    socket.on('init-room', handler);
    rH = handler;
  };

  ChatApp.messageHandler = function(handler) {
    // Handler prototype function (time, from, room, msg)
    socket.on('message', handler);
    mH = handler;
  };

  ChatApp.send = function (room, msg, callback) {
    // Callback(boolean) get true or false
    socket.emit('message', room, msg, callback);
  };

  ChatApp.setUsername = function (username, callback) {
    // Callback(boolean) get true or false
    socket.emit('username', username, callback);
    saved_username = username;
  };

  ChatApp.join = function (channel, callback) {
    // Callback(boolean) get true or false
    socket.emit('join', channel, callback);
  };

  ChatApp.leave = function (channel, callback) {
    // Callback(boolean) get true or false
    socket.emit('leave', channel, callback);
  };

  ChatApp.pause = function (channel, callback) {
    // Callback(boolean) get true or false
    socket.emit('pause', channel, callback);
  };

  ChatApp.resume = function (channel, callback) {
    // Callback(boolean) get true or false
    socket.emit('resume', channel, callback);
  };

  ChatApp.sendLastTimeStamp = function (lastTimestamp) {
    socket.emit('last-timestamp', lastTimestamp);
  };
})(io, ChatApp);
