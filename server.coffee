###
  command line
   --port [number] defaut 3001
   --connect http://something:port
###

opt =
  default:
    port: 3001

argv = (require 'minimist') (process.argv.slice 2), opt

socketio = require 'socket.io'
_ = require 'lodash'
io = socketio argv.port

GLOBAL_USERNANE = ['admin', 'root', 'sysadmin', '_server', '__']
GLOBAL_CHATROOM = {}
GLOBAL_SERVER = []

server_broadcast = (time, from, room, msg) ->
  for server in GLOBAL_SERVER
    server.socket.emit 'server-message', time, from, room, msg
  return

server_action_broadcast = (type, user, name) ->
  for server in GLOBAL_SERVER
    server.socket.emit 'server-action', type, user, name
  return

handle_server_broadcast = (time, from, name, msg) =>
  console.log 'receive broadcast', from, name, msg
  if name not of GLOBAL_CHATROOM
    GLOBAL_CHATROOM[name] = new Chatroom name
  room = GLOBAL_CHATROOM[name]
  room.message time, from, msg
  return

handle_server_action = (type, user, name) ->
  c = findClientByName user
  if not c
    c = new Client false
    c.setName user
    clients.push c
  console.log type, user, name
  switch type
    when 'join' then c.join name
    when 'leave' then c.leave name
    when 'pause' then c.pause name
    when 'resume' then c.resume name
  return

class Server
  constructor: (@socket) ->
    # attach server message handler
    @socket.on 'server-message', handle_server_broadcast
    @socket.on 'server-action', handle_server_action
    @socket.on 'disconnect', =>
      console.log('*** Main Server Down. ***')
      _.pull GLOBAL_SERVER, this


class Chatroom
  constructor: (@name) ->
    @messages = []
    @clients = []
    @pause_time = {}

  join: (client) ->
    @clients.push client
    return

  sendAllMessages: (client) ->
    @messages.forEach (m) =>
      client.send m[0], m[1], @name, m[2]
    return

  sendTimedMessages: (client, t) ->
    @messages.forEach (m) =>
      if m[0] > t
        client.send m[0], m[1], @name, m[2]
    return

  message: (time, from, msg) ->
    @messages.push [time, from, msg]
    @clients.forEach (client) =>
      if client.name not of @pause_time or @pause_time[client.name] < 0
        client.send time, from, @name, msg
      return
    return

  pause: (client, fn) ->
    name = client.name
    if name == '__'
      return false
    if name not of @pause_time or @pause_time[name] < 0
      @pause_time[name] = @messages.length
      return true
    else
      return false

  resume: (client) ->
    name = client.name
    if name == '__'
      return false
    if name not of @pause_time or @pause_time[name] < 0
      return false

    i = @pause_time[name]

    while i < @messages.length
      m = @messages[i]
      client.send m[0], m[1], @name, m[2]
      i++
    @pause_time[name] = -1

    return true


  leave: (client) ->
    _.pull @clients, client
    return

class Client
  constructor: (@socket) ->
    @name = '__'
    @room = []
    @server = false
    console.log 'New user:', @name
    @bindSocket @socket
    @last_timestamp = -1

  join: (name) ->
    if name not of GLOBAL_CHATROOM
      GLOBAL_CHATROOM[name] = new Chatroom name
    room = GLOBAL_CHATROOM[name]
    room.join this
    @room.push room
    console.log 'Client', @name, 'join', name
    true

  leave: (name) ->
    if name not of GLOBAL_CHATROOM
      return false
    room = GLOBAL_CHATROOM[name]
    room.message _.now(), '_server', (@name + ' left the room.')
    room.leave this
    _.pull @room, room
    true

  pause: (name) ->
    if name not of GLOBAL_CHATROOM
      return false
    GLOBAL_CHATROOM[name].pause this

  resume: (name) ->
    if name not of GLOBAL_CHATROOM
      return false
    GLOBAL_CHATROOM[name].resume this

  bindSocket: (socket) ->
    if not socket
      return

    # Ping-Pong
    @socket.on 'x-ping', (fn) ->
      console.log 'ping\'d!'
      fn 'pong'
      return

    # Join
    @socket.on 'join', (name, fn) =>
      server_action_broadcast 'join', @name, name
      fn @join name
      return

    # Pause
    @socket.on 'pause', (name, fn) =>
      server_action_broadcast 'pause', @name, name
      fn @pause name
      return

    # Resume
    @socket.on 'resume', (name, fn) =>
      server_action_broadcast 'resume', @name, name
      fn @resume name
      return

    # Leave
    @socket.on 'leave', (name, fn) =>
      server_action_broadcast 'leave', @name, name
      fn @leave name
      return

    # Message
    @socket.on 'message', (name, msg, fn) =>
      if name not of GLOBAL_CHATROOM
        fn false
        return
      t = _.now()
      GLOBAL_CHATROOM[name].message t, @name, msg
      server_broadcast t, @name, name, msg # time, from, room, message
      console.log 'Message from', @name, 'to room', name, ':', msg
      fn true
      return

    # Last timestramp
    @socket.on 'last-timestamp', (timestamp) ->
      @last_timestamp = timestamp
      return

  ping: ->
    if not @socket
      return
    @clientTimeout = setTimeout (=> @disconnect()), 5000
    @socket.emit 'x-ping', (res) =>
      clearTimeout @clientTimeout
      return
    return

  send: (time, from, room, msg) ->
    if not @socket
      return
    @socket.emit 'message', time, from, room, msg
    return

  initRoom: () ->
    roomNames = []
    for r in @room
      roomNames.push r.name
    console.log roomNames
    @socket.emit 'init-room', roomNames
    if @last_timestamp > 0
      for r in roomNames
        r.sendTimedMessage this, @last_timestamp
    return

  setName: (name) ->
    if _.indexOf(GLOBAL_USERNANE, name) != -1
      return false
    if @name != '__'
      _.pull GLOBAL_USERNANE, @name
    GLOBAL_USERNANE.push name
    @name = name
    true

  disconnect: ->
    # remove old socket?
    return

  comeBack: (socket) ->
    console.log 'Comeback user:', @name
    @socket = socket
    @bindSocket socket
    @initRoom()
    return

clients = []

findClientBySocket = (socket) ->
  for c in clients
    if c.socket == socket
      return c
  false

findClientByName = (name) ->
  for c in clients
    if c.name == name
      return c
  false

io.on 'connection', (socket) ->
  console.log 'New connection.'

  # Attach server
  socket.on 'server', () ->
    console.log 'New server'
    GLOBAL_SERVER.push new Server socket
    return

  socket.on 'username', (name, fn) =>
    c = findClientByName name
    if c
      c.comeBack socket
    else
      console.log 'Set username', name
      client = new Client socket
      client.setName name
      clients.push client
    fn true
    return

  socket.on 'disconnect', ->
    c = findClientBySocket socket
    if c
      c.disconnect()

console.log 'Running on http://localhost:' + argv.port

if argv.connect
  console.log 'Connecting to', argv.connect
  client = require 'socket.io-client'
  socket = client argv.connect
  socket.on 'connect', () ->
    socket.emit 'server'
    GLOBAL_SERVER.push new Server socket
    return
  socket.on 'reconnect', () ->
    socket.emit 'server'
    return
